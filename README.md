# Infinigame README

Welcome to the README for **Infinigame**! This document provides an overview of the game's architecture, scripts, how information is transmitted, and what developers need to know to work on the project effectively.

## Table of Contents

1. [Overview](#overview)
2. [Gameplay Overview](#gameplay-overview)
3. [Scripts](#scripts)
4. [Information Flow](#information-flow)
5. [Developer Guidelines](#developer-guidelines)
6. [Additional Resources](#additional-resources)

## Overview

**Infinigame** is a top-down shooter game where players control a character to fight against waves of enemies. The game features leveling up, various buffs, and dynamic combat mechanics.

## Scripts

Here's a breakdown of the main scripts in the game and their functionalities:

### GameManager

- **Description:** Manages game states, such as pausing and unpausing the game.
- **Functionality:** Controls the overall game flow, manages UI elements, and handles pause states.

### Functions

- **Description:** Contains various static methods for modifying player stats, interacting with enemies, shooting, and handling buffs.
- **Functionality:** Centralizes the game's mechanics, including player-enemy interactions, shooting logic, buff management, and more.

### Stats

- **Description:** Manages player and enemy stats, such as health, damage, attack speed, and movement speed.
- **Functionality:** Tracks and updates character stats, handles level progression, and stores information related to character attributes.

### PlayerShooting

- **Description:** Enables the player character to shoot projectiles at enemies.
- **Functionality:** Handles shooting mechanics, including aiming, firing, and controlling projectile behavior. (See "Shoot" function for detailed informations).

### Buffs

- **Description:** Defines the different types of buffs available in the game.
- **Functionality:** Lists the available buff types, allowing for easy reference and management of buffs.

### IFlags

- **Description:** Implements a flag system for character status effects.
- **Functionality:** Enables characters to have various flags indicating status effects like multi-shot or stun.

## Information Flow

1. **Player Input:** Player input is received through Unity's input system.
2. **Player Actions:** Player actions, such as shooting and movement, are handled by the `PlayerShooting` and `Functions` scripts. `GameManager` calls the `Shoot` function based on the **Player's** attack speed.
3. **Enemy Behavior:** Enemy behavior and interactions with the player are managed by the `Functions` script. //TODO atm they only chase the player.
4. **Buffs:** Buffs are applied using the `AddBuff` method in the `Functions` script.
5. **Flags:** Flags are applied using the `AddFlag` method in the `Functions` script. You can remove `Flags` using `RemoveFlag` or you can check if a `unit` has a flag using `HasFlag` which returns `bool`.
6. **Leveling Up:** Player leveling up is managed by the `LevelUpPlayer` method in the `Functions` script.
7. **UI Updates:** UI elements, such as health bars and stats, are updated using the `UpdateStatsText` method in the `Functions` script.
8. **Gaining stats:** On every `level up`, the `Player` gets to choose from 3 randomly selected `Buffs` from a menu. Once the `Player` selects a `Buff`, the game resumes. Those `"Buffs"` can vary, from `movement speed` to `attack speed`, `health`, `damage` and even new `Power-Ups` that can alter the game's main flow. 
9. **Ease of use:** The script logic was thinked in a way that you dont need to write hundreds of  lines of code for every new thing, this way, the usage of already present `Functions` is recommended. The `"amount"` of power you gain from `Buff selection screen` is directly read from the `Buff Descriptions` dictionary , processed as **%** **(percentage)** and send directly into `AddBuff` function, as float values. *(For example: This buff gives you 10% more damage. -> 10% is directly read from that description, processed as float percentage = 10 -> percentage = 1.10 -> passed to AddBuff)*

## Developer Guidelines

- Familiarize yourself with the architecture by reading the script descriptions and understanding their responsibilities.
- Use the `Functions` script as a central hub for game mechanics to keep code organized and modular. `Functions` will handle almost every aspect of the game. 
- When adding new buffs or mechanics, consider extending the `Buffs` dictionary in the `Buffs.cs` script to handle new functionalities.

## Additional Resources
**This project is under a non-comercial license. **
**Check Project -> License or directly** https://gitlab.com/Gustopop/infinigame/-/blob/master/LICENSE
