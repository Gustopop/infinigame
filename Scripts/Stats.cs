using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Stats : MonoBehaviour
{
    public float damage = 50;
    public float armorPen = 0;
    public float magicPen = 0;
    public float maxHP = 100;
    public float currentHP;
    public float healthRegen = 0;
    public float attackSpeedBase = 1f;
    public float attackSpeed = 1f;
    public float lifesteal = 0;
    public float armor = 0;
    public float magicResist = 0;
    public float xp = 0;
    public float level = 1;
    public float moveSpeedBase = 5f;
    public float moveSpeed = 5f;
    public float bonusXP = 0;
    public float baseXPgain = 10f;
}

