using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;

    private void Update()
    {
        if (player != null)
        {
            Vector3 playerPosition = player.position;
            Vector3 cameraPosition = new Vector3(playerPosition.x, playerPosition.y, transform.position.z);
            transform.position = cameraPosition;
        }
    }
}
