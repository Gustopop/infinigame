using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BuffType
{
    MaxHP,
    HealthRegen,
    Heal,
    Stun,
    AttackSpeed,
    MoveSpeed,
    BonusXP,
    Slow,
    MultiShot,
    Damage,
    AuraBurn,
}

public static class Buffs
{
    public static Dictionary<BuffType, string> buffDescriptions = new Dictionary<BuffType, string>
    {
        { BuffType.MaxHP, "Increase your maximum HP by 5%." },
        { BuffType.HealthRegen, "Regenerate your health by 1% of your Max HP /5 seconds." },
        { BuffType.Heal, "Instant heal. Restores 20% of your maximum HP." },
        { BuffType.Stun, "Stun a random enemy periodically" },
        { BuffType.AttackSpeed, "Increase your attack speed by 20%." },
        { BuffType.MoveSpeed, "Movement speed + 10%." },
        { BuffType.BonusXP, "Increase your XP gains by 10%." },
        { BuffType.Slow, "Slow any enemy hit by 20%. Duration: 1 second." },
        { BuffType.MultiShot, "Projectile penetration +1." },
        { BuffType.Damage, "Increase your flat damage by 10%." },
        { BuffType.AuraBurn, "Burn enemies near you for 10% of your flat damage.(Hover this to see the range)" },
    };

    private static Dictionary<BuffType, Sprite> buffImages = new Dictionary<BuffType, Sprite>
    {
        { BuffType.MaxHP, Resources.Load<Sprite>("BuffImages/MaxHP") },
        { BuffType.HealthRegen, Resources.Load<Sprite>("BuffImages/HealthRegen") },
        { BuffType.Heal, Resources.Load<Sprite>("BuffImages/Heal") },
        { BuffType.Stun, Resources.Load<Sprite>("BuffImages/Stun") },
        { BuffType.AttackSpeed, Resources.Load<Sprite>("BuffImages/AttackSpeed") },
        { BuffType.MoveSpeed, Resources.Load<Sprite>("BuffImages/MoveSpeed") },
        { BuffType.BonusXP, Resources.Load<Sprite>("BuffImages/XP") },
        { BuffType.Slow, Resources.Load<Sprite>("BuffImages/Slow") },
        { BuffType.MultiShot, Resources.Load<Sprite>("BuffImages/Multishot") },
        { BuffType.Damage, Resources.Load<Sprite>("BuffImages/Damage") },
        { BuffType.AuraBurn, Resources.Load<Sprite>("BuffImages/AuraBurn") },
    };


    public static string GetBuffDescription(BuffType buff)
    {
        if (buffDescriptions.TryGetValue(buff, out string description))
        {
            return description;
        }
        return "Description not available";
    }

    public static Sprite GetBuffImage(BuffType buff)
    {
        // Retrieve the image reference from the dictionary
        if (buffImages.TryGetValue(buff, out Sprite image))
        {
            return image;
        }
        return null;
    }
}