using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FlagType
{
    None,
    Immune,
    Stunned,
    Buffed,
    MultiShot,
}

public class IFlags : MonoBehaviour
{
    public List<FlagType> flags = new List<FlagType>();

    public void AddFlag(FlagType flag)
    {
        if (!flags.Contains(flag))
        {
            flags.Add(flag);
        }
    }

    public void RemoveFlag(FlagType flag, GameObject unit)
    {
        flags.Remove(flag);
        Stats unitStats = unit.GetComponent<Stats>();
        switch (flag)
        {
            case FlagType.Immune:
                break;
            case FlagType.Stunned:
                unitStats.moveSpeed = 3;
                break;
            case FlagType.Buffed:
                break;
            case FlagType.MultiShot:
                break;
            default:
                break;
        }
    }
}
