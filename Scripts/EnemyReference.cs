using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyReference : MonoBehaviour
{
    public static EnemyReference Instance { get; private set; }

    public Stats EnemyStats { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        EnemyStats = GetComponent<Stats>();
    }
}
