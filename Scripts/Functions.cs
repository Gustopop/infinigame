using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;
using static GameManager;
using static Buffs;

public static class Functions
{
    public static Slider levelProgressSlider;
    
    public static List<BuffType> currentBuffOptions;
    public static GameObject buffSelectionPanel;
    public static GameObject buffOptionButtonPrefab;
    public static string percentagePattern = @"\d+(\.\d+)?%";

    
    
    public static void AddBuff(BuffType buffType, float amount, GameObject unit)
    {
        Stats stats = unit.GetComponent<Stats>();
        if (stats != null)
        {
            switch (buffType)
            {
                case BuffType.MaxHP:
                    stats.maxHP *= amount;
                    break;
                case BuffType.HealthRegen:
                    //TODO health regen
                    break;
                case BuffType.Heal:
                    stats.currentHP += stats.maxHP * amount/100f;
                    break;
                case BuffType.Stun:
                    stats.moveSpeed = 0;
                    AddFlag(FlagType.Stunned, unit);
                    break;
                case BuffType.AttackSpeed:
                    stats.attackSpeed += stats.attackSpeedBase * amount / 100;
                    break;
                case BuffType.MoveSpeed:
                    stats.moveSpeed += stats.moveSpeedBase * amount / 100;
                    break;
                case BuffType.BonusXP:
                    stats.bonusXP += amount;
                    break;
                case BuffType.Slow:
                    stats.moveSpeed -= stats.moveSpeedBase * amount / 100;
                    //TODO slow duration
                    break;
                case BuffType.MultiShot:
                    AddFlag(FlagType.MultiShot, unit);
                    break;
                case BuffType.Damage:
                    stats.damage *= amount;
                    break;
                case BuffType.AuraBurn:
                    //TODO Aura burn
                    break;
                default:
                    break;
            }
            UpdateStatsText(Player.GetComponent<Stats>());
        }
        else
        {
            Debug.LogWarning("Stats component not found on the provided GameObject.");
        }
    }


    public static GameObject GetClosestUnit(PlayerShooting playerShooting)
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float closestDistance = Mathf.Infinity;
        Vector3 playerPosition = playerShooting.transform.position;
        GameObject closestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(playerPosition, enemy.transform.position);

            if (distanceToEnemy < closestDistance)
            {
                closestEnemy = enemy;
                closestDistance = distanceToEnemy;
            }
        }
        

        return closestEnemy;
    }
    
    
    public static void Shoot(GameObject bulletPrefab, float bulletSpeed, GameObject closestEnemy, float attackSpeed, Transform playerTransform)
    {
        if (bulletPrefab != null && closestEnemy != null)
        {
            Vector3 bulletDirection = (closestEnemy.transform.position - playerTransform.position).normalized;

            // Calculate the angle between the player and the enemy
            float angle = Mathf.Atan2(bulletDirection.y, bulletDirection.x) * Mathf.Rad2Deg;

            // Create the bullet instance with the calculated rotation
            GameObject bulletInstance = GameObject.Instantiate(bulletPrefab, playerTransform.position, Quaternion.Euler(0f, 0f, angle));
            bulletInstance.GetComponent<Rigidbody2D>().velocity = bulletDirection * bulletSpeed;
            
        }
    }
    
    public static void AddParticle(GameObject particlePrefab, Vector3 position)
    {
        GameObject explosion = GameObject.Instantiate(particlePrefab, position, Quaternion.identity);
        GameObject.Destroy(explosion, explosion.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length);
    }
    
    public static void AddFlag(FlagType flag, GameObject unit)
    {
        IFlags flagComponent = unit.GetComponent<IFlags>();
        if (flagComponent == null)
        {
            flagComponent = unit.AddComponent<IFlags>();
        }
        if (flagComponent != null)
        {
            flagComponent.AddFlag(flag);
        }
        else
        {
            Debug.LogWarning("IFlags component not found on the provided GameObject.");
        }
    }

    public static void RemoveFlag(FlagType flag, GameObject unit)
    {
        IFlags flagComponent = unit.GetComponent<IFlags>();
        if (flagComponent != null)
        {
            flagComponent.RemoveFlag(flag, unit);
        }
        else
        {
            Debug.LogWarning("IFlags component not found on the provided GameObject.");
        }
    }

    public static bool HasFlag(FlagType flag, GameObject gameObject)
    {
        IFlags flagComponent = gameObject.GetComponent<IFlags>();
        if (flagComponent != null)
        {
            return flagComponent.flags.Contains(flag);
        }

        return false;
    }
    
    public static void TakeDamage(float damage, GameObject unit, GameObject attacker)
    {
        Stats stats = unit.GetComponent<Stats>();
        if (stats != null)
        {
            stats.currentHP -= damage;
        }
        else
        {
            Debug.LogWarning("Stats component not found on the provided GameObject.");
        }
        if (stats.currentHP <= 0)
        {
            OnKillUnit(attacker, unit);
        }
    }

    public static void OnKillUnit(GameObject player, GameObject enemy)
    {
        Stats playerStats = player.GetComponent<Stats>();
        levelProgressSlider = GameObject.Find("LevelProgressSlider").GetComponent<Slider>();
        GameObject.Destroy(enemy);
        playerStats.xp += playerStats.baseXPgain + playerStats.baseXPgain * playerStats.bonusXP / 100f;
        levelProgressSlider.value = playerStats.xp;
        if (playerStats.xp >= 100 * playerStats.level)
        {
            LevelUpPlayer(player);
            LevelUpEnemy(GameObject.FindWithTag("Enemy"));
            UpdateStatsText(playerStats);
        }
    }

    public static void LevelUpPlayer(GameObject player)
    {
        Stats playerStats = player.GetComponent<Stats>();
        TextMeshProUGUI levelText = GameObject.Find("LevelText").GetComponent<TextMeshProUGUI>();
        levelProgressSlider = GameObject.Find("LevelProgressSlider").GetComponent<Slider>();

        playerStats.damage += 10;
        playerStats.level += 1;
        playerStats.xp = 0;
        playerStats.maxHP += 10;
        playerStats.currentHP = playerStats.maxHP;
        playerStats.attackSpeed += 0.1f;
        playerStats.moveSpeed += 0.1f;
        
        levelProgressSlider.maxValue = 100 * playerStats.level;
        levelProgressSlider.value = playerStats.xp;
        levelText.text = playerStats.level.ToString();
        InitiateBuffSelection();
        UpdateStatsText(playerStats);

    }
    public static void LevelUpEnemy(GameObject enemy)
    {
        Stats enemyStats = enemy.GetComponent<Stats>();
        enemyStats.level += 1;
        enemyStats.maxHP += 10;
        enemyStats.currentHP = enemyStats.maxHP;
        enemyStats.attackSpeed += 0.1f;
        enemyStats.moveSpeed += 0.1f;
    }

    public static void UpdateStatsText(Stats playerStats)
    {
        TextMeshProUGUI DamageTextValue = GameObject.Find("DamageTextValue").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI ASTextValue = GameObject.Find("ASTextValue").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI MSTextValue = GameObject.Find("MSTextValue").GetComponent<TextMeshProUGUI>();
        DamageTextValue.text = Math.Round(playerStats.damage, 0).ToString();
        ASTextValue.text = Math.Round(playerStats.attackSpeed, 2).ToString();
        MSTextValue.text = Math.Round(playerStats.moveSpeed, 2).ToString();
    }
    
    
    public static void InitializeBuffSelection(GameObject panel, GameObject buttonPrefab)
    {
        buffSelectionPanel = panel;
        buffOptionButtonPrefab = buttonPrefab;
        buffSelectionPanel.SetActive(false);
        buttonPrefab.SetActive(false);
    }
    
    

    private static void InitiateBuffSelection()
    {
        // Pause the game
        Time.timeScale = 0;
        GMInstance.SetGamePaused(true);

        // Generate random buff options
        currentBuffOptions = GenerateRandomBuffOptions(3);

        // Display the buff selection UI panel
        DisplayBuffSelectionPanel();
    }

    private static void DisplayBuffSelectionPanel()
    {
        buffSelectionPanel.SetActive(true);

        int totalOptions = currentBuffOptions.Count;
        for (int i = 0; i < currentBuffOptions.Count; i++)
        {
            CreateBuffOptionButton(currentBuffOptions[i], i, totalOptions);
        }

    }
    
    private static void CreateBuffOptionButton(BuffType buff, int index, int totalOptions)
    {
        GameObject button;

        // Check if the button already exists
        if (index < buffSelectionPanel.transform.childCount)
        {
            // If it exists, reuse the existing button
            button = buffSelectionPanel.transform.GetChild(index).gameObject;
            button.SetActive(true); // Make sure it's active
        }
        else
        {
            // If it doesn't exist, instantiate a new button
            button = GameObject.Instantiate(buffOptionButtonPrefab, buffSelectionPanel.transform);
            button.SetActive(true);
        }

        // Calculate the position based on the button index
        float centerX = 0;
        float offsetX = (index - (totalOptions - 1) / 2.0f) * 300; // Adjust this value as needed
        button.GetComponent<RectTransform>().anchoredPosition = new Vector2(centerX + offsetX, 0);

        // Get the button components
        Text buttonText = button.GetComponentInChildren<Text>();
        Image buttonImage = button.GetComponentInChildren<Image>();
        Text descriptionText = button.transform.Find("Description").GetComponent<Text>();
        Image descriptionImage = button.transform.Find("DescriptionImage").GetComponent<Image>();

        // Set the button's text and description based on the buff
        buttonText.text = "Select";
        descriptionText.text = GetBuffDescription(buff);
        buttonImage.sprite = GetBuffImage(buff);
        descriptionImage.sprite = GetBuffImage(buff);

        // Attach a click event to the button
        Button buttonComponent = button.GetComponent<Button>();
        buttonComponent.onClick.RemoveAllListeners(); // Remove existing listeners
        buttonComponent.onClick.AddListener(() => OnBuffOptionSelected(buff));
    }



    private static void OnBuffOptionSelected(BuffType selectedBuff)
    {
        // Apply the selected buff to the player
        ApplyBuff(selectedBuff);

        // Resume the game
        Time.timeScale = 1;
        GMInstance.SetGamePaused(false);

        // Remove the buff selection UI panel
        buffSelectionPanel.SetActive(false);
    }

    private static void ApplyBuff(BuffType buff)
    {
        float percentage = ExtractPercentage(buffDescriptions[buff]);
        percentage = 1 + percentage / 100f;

        AddBuff(buff, percentage, Player);
    }
    private static float ExtractPercentage(string description)
    {
        Match match = Regex.Match(description, percentagePattern);
        
        if (match.Success)
        {
            string percentageString = match.Value.Replace("%", "");
            return float.Parse(percentageString);
        }
        else
        {
            return 0f; 
        }
    }

    private static List<BuffType> GenerateRandomBuffOptions(int count)
    {
        List<BuffType> allBuffs = new List<BuffType>((BuffType[])Enum.GetValues(typeof(BuffType)));

        allBuffs.RemoveAll(buff => buff == BuffType.Slow || buff == BuffType.Stun);

        List<BuffType> shuffledBuffs = ShuffleList(allBuffs);

        List<BuffType> selectedBuffs = shuffledBuffs.GetRange(0, count);

        return selectedBuffs;
    }


    private static List<T> ShuffleList<T>(List<T> list)
    {
        List<T> shuffledList = new List<T>(list);
        int n = shuffledList.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = shuffledList[k];
            shuffledList[k] = shuffledList[n];
            shuffledList[n] = value;
        }
        return shuffledList;
    }



    // Add more static methods to modify other stats as needed.
}

