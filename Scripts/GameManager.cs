using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Functions;

public class GameManager : MonoBehaviour
{
    public static GameManager GMInstance { get; private set; } // Singleton pattern
    public GameObject enemyPrefab;
    public float spawnDelay = 1f;
    public float spawnRadius = 5f;
    public float minSpawnDistance = 3f;
    public Transform player;
    public Slider levelProgressSlider;
    public static GameObject Player { get; private set; }
    public Stats playerStats;
    private static GameObject buffSelectionPanel;
    private static GameObject buffOptionButtonPrefab;
    
    private bool isGamePaused = false;


    private void Awake()
    {
        GMInstance = this;
        Player = GameObject.FindGameObjectWithTag("Player");
        levelProgressSlider = GameObject.Find("LevelProgressSlider").GetComponent<Slider>();
        TextMeshProUGUI DamageTextValue = GameObject.Find("DamageTextValue").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI ASTextValue = GameObject.Find("ASTextValue").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI MSTextValue = GameObject.Find("MSTextValue").GetComponent<TextMeshProUGUI>();
        buffSelectionPanel = GameObject.Find("BuffPanel");
        buffOptionButtonPrefab = GameObject.Find("BuffButton");
        
        Player.AddComponent<Stats>();
        playerStats = Player.GetComponent<Stats>();
        levelProgressSlider.maxValue = 100 * playerStats.level;
        levelProgressSlider.value = playerStats.xp;
        DamageTextValue.text = playerStats.damage.ToString();
        ASTextValue.text = playerStats.attackSpeed.ToString();
        MSTextValue.text = playerStats.moveSpeed.ToString();
    }

    private void Start()
    {
        AddFlag(FlagType.Immune, enemyPrefab.gameObject);
        AddBuff(BuffType.Stun, 0, enemyPrefab.gameObject);
        StartCoroutine(SpawnEnemiesCoroutine());
        InitializeBuffSelection(buffSelectionPanel, buffOptionButtonPrefab);
    }

    private IEnumerator SpawnEnemiesCoroutine()
    {
        float baseSpawnDelay = 1f; // Set your desired base spawn delay here

        while (true)
        {
            if (!isGamePaused)
            {
                SpawnEnemy();
            }
        
            float adjustedSpawnDelay = baseSpawnDelay - playerStats.level * 0.1f;
            adjustedSpawnDelay = Mathf.Max(adjustedSpawnDelay, 0.2f);

            yield return new WaitForSeconds(adjustedSpawnDelay);
        }
    }

// Call this function to pause or resume the game
    public void SetGamePaused(bool paused)
    {
        isGamePaused = paused;
    
        // Pause or resume the spawning coroutine based on the pause state
        if (paused)
        {
            StopCoroutine(SpawnEnemiesCoroutine());
        }
        else
        {
            StartCoroutine(SpawnEnemiesCoroutine());
        }
    }

    public void SpawnEnemy()
    {
        if (enemyPrefab != null && player != null)
        {
            float spawnDistance = spawnRadius;
            Vector3 spawnPosition;
            float distanceToPlayer;

            int maxAttempts = 10;
            int attempts = 0;

            do
            {
                Vector2 randomOffset = Random.insideUnitCircle * spawnDistance;
                spawnPosition = player.position + new Vector3(randomOffset.x, randomOffset.y, 0f);

                distanceToPlayer = Vector3.Distance(spawnPosition, player.position);

                if (distanceToPlayer < minSpawnDistance)
                {
                    spawnDistance += 1f;
                }

                attempts++;
            }
            while (distanceToPlayer < minSpawnDistance && attempts < maxAttempts);

            if (attempts == maxAttempts)
            {
                spawnPosition = player.position + new Vector3(minSpawnDistance, 0f, 0f);
            }

            GameObject enemyInstance = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);

            Stats enemyStats = enemyInstance.GetComponent<Stats>();
            if (enemyStats == null)
            {
                enemyStats = enemyInstance.AddComponent<Stats>();
            }
            RemoveFlag(FlagType.Immune, enemyInstance);
            RemoveFlag(FlagType.Stunned, enemyInstance);
            AddFlag(FlagType.Buffed, enemyInstance);
            enemyStats.currentHP = enemyStats.maxHP;

        }
    }
}
