using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Transform player;
    private Stats enemyStats;

    private void Awake()
    {
        enemyStats = GetComponent<Stats>();
    }

    private void Update()
    {
        if (player != null && enemyStats != null)
        {
            float modifiedMoveSpeed = enemyStats.moveSpeed; // Use the modified moveSpeed here if you have any buffs/debuffs
            Vector3 direction = (player.position - transform.position).normalized;
            Vector3 movement = direction * modifiedMoveSpeed * Time.deltaTime;
            transform.position += movement;
        }
    }

}