using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Functions;

public class PlayerShooting : MonoBehaviour
{
    public static Stats playerStats;

    public StatsManager statsManager;

    public GameObject bulletPrefab;
    public float bulletSpeed = 10f;
    private float currentCooldown = 0f;
    private GameObject closestEnemy;



    private void Start()
    {
        playerStats = GetComponent<Stats>();
        playerStats.currentHP = playerStats.maxHP; 
        statsManager = GameObject.Find("GameManager").GetComponent<StatsManager>();
        
    }
    
    private void Update()
    {
        closestEnemy = GetClosestUnit(this);

        if (closestEnemy != null)
        {
            if (currentCooldown <= 0f)
            {
                Shoot(bulletPrefab, 10f, closestEnemy, playerStats.attackSpeed, transform);
                currentCooldown = 1f / playerStats.attackSpeed;
            }
        }

        currentCooldown -= Time.deltaTime;
    }
    

}

