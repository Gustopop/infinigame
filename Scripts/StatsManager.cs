using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
    public static Stats playerStats;

    public void AddXP(int amount)
    {
        playerStats.xp += amount;
    }

    public void ModifyHP(int amount)
    {
        playerStats.currentHP += amount;

        playerStats.currentHP = Mathf.Clamp(playerStats.currentHP, 0, playerStats.maxHP);

        if (playerStats.currentHP <= 0)
        {
        }
    }

}
