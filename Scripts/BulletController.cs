using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Functions;
using static GameManager;

public class BulletController : MonoBehaviour
{
    public float lifespan = 10f;
    public GameObject explosionPrefab;
    public Stats playerStats;
    public int shotCount = 0;

    private void Start()
    {
        playerStats = Player.GetComponent<Stats>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            if (!HasFlag(FlagType.Immune, other.gameObject))
            {
                if (!HasFlag(FlagType.MultiShot, Player))
                {
                    Destroy(gameObject);
                    AddParticle(explosionPrefab, other.transform.position);
                    TakeDamage(playerStats.damage, other.gameObject, Player);
                }
                else if (HasFlag(FlagType.MultiShot, Player) && shotCount <=2)
                {
                    shotCount++;
                    AddParticle(explosionPrefab, other.transform.position);
                    TakeDamage(playerStats.damage, other.gameObject, Player);
                }
                if (shotCount == 2)
                {
                    Destroy(gameObject);
                    shotCount = 0;
                }
            }
            else Console.WriteLine("One enemy has IMMUNE flag!");
        }
    }
}


